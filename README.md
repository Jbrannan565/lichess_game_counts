# Lichess Game Counts
lichess game counts is a tool to assist players in analyzing what openings they've played.

# Requirements
python3
pip3

# Install
```bash
pip3 install -r requirements.txt
```

# Configuration

```bash
cp example_config.json config.json
```

Then edit config.json to contain the username of interest and one of the following gametypes:
	ultraBullet,
	bullet,
	blitz,
	rapid,
	classical,
	correspondence,
	chess960,
	antichess,
	atomic,
	horde,
	kingOfTheHill,
	racingKings,
	threeCheck

# Usage

```bash
./get_game_counts.py -h
```

Openings should be capitalized. Example: French Defense
