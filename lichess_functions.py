import requests
import sys
import json

def get_user_game_stream(username, user_color, game_type, authtoken):
    '''
    returns a requests stream of the given users games
    '''
    try:
        if authtoken:
            return requests.get('https://lichess.org/api/games/user/{}?opening=true&color={}&perfType={}'.format(
                 username, user_color, game_type
                 ), 
                 stream=True, 
                 headers={
                     "Authorization": "Bearer {}".format(authtoken)
                     })
        else:
            return requests.get('https://lichess.org/api/games/user/{}?opening=true&color={}&perfType={}'.format(
                 username, user_color, game_type
                 ), 
                 stream=True)

    except:
        print("Failed to connect to lichess.")
        sys.exit()

def get_games(color):
    print("Fetching games from lichess\n")
    with open("config.json", "r") as config:
        config = json.loads(config.read())

    if config is None:
        print("Error: Loading config")
        sys.exit()

    if 'username' not in config.keys():
        print("Error: Username not in config")
        sys.exit()

    if 'authtoken' not in config.keys() or config['authtoken'] is None:
        print("This works faster with an authtoken. Check config.")

    user = config['username']
    game_type = config['gametype']

    r = get_user_game_stream(user, color, game_type, config['authtoken'])

    with open("pgns/{}_{}_{}.pgn".format(user, color, game_type), "w+") as f:
        for line in r.iter_lines():
        # filter out keep-alive new lines
            if line:
                decoded_line = line.decode('utf-8')
                if decoded_line[0] == "1":
                    f.write("\n")
                f.write("{}\n".format(decoded_line))
                if decoded_line[0] == "1":
                    f.write("\n")
                    f.write("\n")

