import chess
import chess.pgn
import json

with open("config.json", "r") as config:
    config = json.loads(config.read())

user = config['username']
game_type = config['gametype']

def process_by_opening(opening, player_color):
    pgn = open("pgns/{}_{}_{}.pgn".format(user, player_color, game_type))

    offsets = []

    while True:
        offset = pgn.tell()

        headers = chess.pgn.read_headers(pgn)
        if headers is None:
            break

        if opening in headers.get("Opening", "?"):
            print("{: <60} : {}".format(headers.get("Opening", "?"), headers.get("Site", "?")))
            offsets.append(offset)

    print("\n{} games played: {}".format(opening, len(offsets)))

def process_by_first_moves(first_move_white, first_move_black, player_color):
    if len(first_move_white) < 3 or len(first_move_black) < 3:
        print("Error: please include from square for pawn moves. (Ex: d2d4")
        return

    pgn = open("pgns/{}_{}_{}.pgn".format(user, player_color, game_type))

    offsets = []

    while True:
        offset = pgn.tell()

        game = chess.pgn.read_game(pgn)
        if game is None:
            break

        for idx, move in enumerate(game.mainline_moves()):
            if idx == 0 and first_move_white not in move.uci():
                break
            elif idx == 1 and first_move_black in move.uci():
                print("{: <60} : {}".format(game.headers.get("Opening", "?"), game.headers.get("Site", "?")))
                offsets.append(offset)
                offsets.append(offset)
            elif idx > 1:
                break

    print("\ngames that started 1. {} {}: {}".format(first_move_white, first_move_black, len(offsets)))

def process_by_black_move(first_move_black, player_color):
    if len(first_move_black) < 3:
        print("Error: please include from square for pawn moves. (Ex: d2d4)")
        return

    pgn = open("pgns/{}_{}_{}.pgn".format(user, player_color, game_type))

    offsets = []

    while True:
        offset = pgn.tell()

        game = chess.pgn.read_game(pgn)
        if game is None:
            break

        for idx, move in enumerate(game.mainline_moves()):
            if idx == 1 and first_move_black in move.uci():
                print("{: <60} : {}".format(game.headers.get("Opening", "?"), game.headers.get("Site", "?")))
                offsets.append(offset)
            elif idx > 1:
                break

    print("\ngames that started 1. ... {}: {}".format(first_move_black, len(offsets)))

def process_by_white_move(first_move_white, player_color):
    if len(first_move_white) < 3:
        print("Error: please include from square for pawn moves. (Ex: d2d4)")
        return

    pgn = open("pgns/{}_{}_{}.pgn".format(user, player_color, game_type))

    offsets = []

    while True:
        offset = pgn.tell()

        game = chess.pgn.read_game(pgn)
        if game is None:
            break

        for idx, move in enumerate(game.mainline_moves()):
            if idx == 0 and first_move_white in move.uci():
                print("{: <60} : {}".format(game.headers.get("Opening", "?"), game.headers.get("Site", "?")))
                offsets.append(offset)
            elif idx > 1:
                break

    print("\ngames that started 1. {}: {}".format(first_move_white, len(offsets)))

def process_by_all_openings(player_color):
    pgn = open("pgns/{}_{}_{}.pgn".format(user, player_color, game_type))

    games = {}

    while True:
        offset = pgn.tell()

        headers = chess.pgn.read_headers(pgn)
        if headers is None:
            break

        opening = headers.get("Opening", "?").split(":")

        if opening[0] in games.keys():
            games[opening[0]].append({
                    'opening': ":".join(opening),
                    'url': headers.get("Site", "?")
                })
        else:
            games[opening[0]] = [
                    {
                        'opening': ":".join(opening),
                        'url': headers.get("Site", "?")
                        }
                    ]

        '''
        if opening in headers.get("Opening", "?"):
            print("{: <60} : {}".format(headers.get("Opening", "?"), headers.get("Site", "?")))
            offsets.append(offset)

    print("\n{} games played: {}".format(opening, len(offsets)))
    '''

    sorted_keys = sorted(games, key=lambda k: len(games[k]), reverse=False)

    for key in sorted_keys:
        print("{} ({}):".format(key, len(games[key])))
        for item in games[key]:
            print("  {: >65} | {}".format(item['opening'], item['url']))
        print("\n")
