#! /usr/bin/env python3

import argparse
import sys
import os
import os.path
from os import path
import json

from game_processing import *
from lichess_functions import get_games

parser = argparse.ArgumentParser(description='Process game counts of a player')
parser.add_argument('--opening', '-o', dest='opening', 
        help="The name of the opening. Capitalize the first letter of every word.")
parser.add_argument('--color', '-c', dest='color', 
        help="The color of the player of interest. Options: black, white, any.", required=True)
parser.add_argument('--first-move-black', '-b', dest='first_move_black', 
        help="The first move of the black player. Specify starting square. Ex: e7e5.")
parser.add_argument('--first-move-white', '-w', dest='first_move_white', 
        help="The first move of the white player. Specify starting square. Ex: e2e4.")
parser.add_argument('--force', '-f', 
        help="Force a refresh of the stored games.", action="store_true")

args = parser.parse_args()

opening = args.opening
color = args.color
first_move_black = args.first_move_black
first_move_white = args.first_move_white

if not path.exists("pgns/"):
    os.mkdir("pgns")

with open("config.json", "r") as config:
    config = json.loads(config.read())
    games_file_name = "pgns/{}_{}_{}.pgn".format(config['username'], color, config['gametype'])
    if not path.exists(games_file_name):
        get_games(color)

if args.force:
    get_games(color)

if opening:
    process_by_opening(opening, color)

elif first_move_black and first_move_white:
    process_by_first_moves(first_move_white, first_move_black, color)

elif first_move_black:
    process_by_black_move(first_move_black, color)

elif first_move_white:
    process_by_white_move(first_move_white, color)

else:
    process_by_all_openings(color)
